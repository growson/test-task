<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    public function provideTestsData()
    {
        return [
            [6, 4, 2, 7, 10],
            [6, 3, 3, 4, 3],
            [4, 3, 3, 2, 0],
        ];
    }

    /**
     * @dataProvider provideTestsData
     */
    public function testSumOneSuccessful($int1, $int2, $divider, $limit, $expected)
    {
        $client = static::createClient();

        $client->request('GET', "/task/1.1/$int1/$int2/$divider/$limit");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $client->getResponse()->getContent());
    }

    /**
     * @dataProvider provideTestsData
     */
    public function testSumTwoSuccessful($int1, $int2, $divider, $limit, $expected)
    {
        $client = static::createClient();

        $client->request('GET', "/task/1.2", [
            'int1' => $int1,
            'int2' => $int2,
            'divider' => $divider,
            'limit' => $limit,
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $client->getResponse()->getContent());
    }

    public function provideTestsDataRange()
    {
        return [
            [1, 2, 2, 7, 2],
            [4, 6, 2, 7, 10],
            [3, 9, 3, 10, 18],
            [3, 8, 3, 2, 0],
        ];
    }

    /**
     * @dataProvider provideTestsDataRange
     */
    public function testSumThreeSuccessful($int1, $int2, $divider, $limit, $expected)
    {
        $client = static::createClient();

        $client->request('GET', "/task/1.3", [
            'int1' => $int1,
            'int2' => $int2,
            'divider' => $divider,
            'limit' => $limit,
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $client->getResponse()->getContent());
    }

    public function testSumThreeFails()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', "/task/1.3", [
            'int1' => 4,
            'int2' => 3,
            'divider' => 2,
            'limit' => 5,
        ]);

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('title:contains("int1 cannot be bigger than int2")')->count() > 0);
    }

    public function provideTestsDataList()
    {
        return [
            [[1, 2, 4], 2, 7, 6],
            [[4, 6, 8], 2, 8, 18],
            [[3, 9, 6], 3, 6, 9],
            [[3, 8, 1], 3, 2, 0],
        ];
    }

    /**
     * @dataProvider provideTestsDataList
     */
    public function testSumFour($values, $divider, $limit, $expected)
    {
        $client = static::createClient();

        $client->request('GET', "/task/1.4", [
            'values' => $values,
            'divider' => $divider,
            'limit' => $limit,
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $client->getResponse()->getContent());
    }
}
