<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends Controller
{
    /**
     * Task 1: PHP/Symfony
     * Option 1: allows to set params as part of URL path
     *
     * @Route("/task/1.1/{int1}/{int2}/{divider}/{limit}")
     *
     * @param $int1
     * @param $int2
     * @param $divider
     * @param $limit
     * @return Response
     */
    public function sumOneAction($int1, $int2, $divider, $limit)
    {
        $sum = 0;

        if ($this->isAccepted($int1, $divider, $limit)) {
            $sum += $int1;
        }
        if ($this->isAccepted($int2, $divider, $limit)) {
            $sum += $int2;
        }

        return new Response($sum);
    }

    /**
     * Task 1: PHP/Symfony
     * Option 2: allows to set params as a query string of URL
     *
     * @Route("/task/1.2")
     *
     * @param Request $request
     * @return Response
     */
    public function sumTwoAction(Request $request)
    {
        return $this->sumOneAction(
            $request->query->getInt('int1'),
            $request->query->getInt('int2'),
            $request->query->getInt('divider'),
            $request->query->getInt('limit')
        );
    }

    /**
     * Task 1: PHP/Symfony
     * Option 3: allows to set params as a query string of URL, and int1/int2 will be used as a range of integers
     *
     * @Route("/task/1.3")
     *
     * @param Request $request
     * @return Response
     */
    public function sumThreeAction(Request $request)
    {
        $int1 = $request->query->getInt('int1');
        $int2 = $request->query->getInt('int2');
        $divider = $request->query->getInt('divider');
        $limit = $request->query->getInt('limit');

        if ($int1 > $int2) {
            throw new \InvalidArgumentException("int1 cannot be bigger than int2");
        }

        $sum = 0;

        for ($i = $int1; $i <= $int2; $i++) {
            if ($this->isAccepted($i, $divider, $limit)) {
                $sum += $i;
            }
        }

        return new Response($sum);
    }

    /**
     * Task 1: PHP/Symfony
     * Option 4: allows to set "values" as array of integers
     *
     * @Route("/task/1.4")
     *
     * @param Request $request
     * @return Response
     */
    public function sumFourAction(Request $request)
    {
        $values = $request->query->get('values');
        $divider = $request->query->getInt('divider');
        $limit = $request->query->getInt('limit');

        $sum = 0;
        foreach ((array) $values as $value) {
            if ($this->isAccepted($value, $divider, $limit)) {
                $sum += $value;
            }
        }

        return new Response($sum);
    }

    protected function isAccepted($value, $divider, $limit)
    {
        return $value > 0 && $value <= $limit && $value % $divider == 0;
    }

    /**
     * Task 2: MySQL
     */
    public function queries()
    {
        // a) An alphabetically-ordered list of the family names of users with more than one telephone number.
        <<<MYSQL
SELECT user.family_name
FROM user WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id) > 1
ORDER BY user.family_name
MYSQL;

        // b) A list of the user IDs of users without a telephone number.
        // option 1
        <<<MYSQL
SELECT user.id
FROM user
WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id) = 0
MYSQL;
        // option 2
        <<<MYSQL
SELECT DISTINCT user.id
FROM user
LEFT JOIN telephone ON telephone.user_id = user.id
WHERE telephone.id IS NULL
MYSQL;

        // c) A alphabetically reverse-ordered list of the given names of users that have a China telephone number.
        // option 1
        <<<MYSQL
SELECT user.id
FROM user
WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id AND telephone.telephone LIKE '+86%') > 0
ORDER BY user.given_name DESC
MYSQL;
        // option 2
        <<<MYSQL
SELECT DISTINCT user.id
FROM user
INNER JOIN telephone ON telephone.user_id = user.id AND telephone.telephone LIKE '+86%'
ORDER BY user.given_name DESC
MYSQL;

        /**
         * @todo: How can we change the schema to avoid the repetition of country codes in the telephone table?
         *
         * We can create an additional table "country"
         * (or maybe "country_phone_code", but in "country" we can store more country related data)
         * and add a field named "phone_code"
         * Next step to add a field "country_id" to table "telephone"
         * After that we can insert countries to country table with their phone codes,
         * then link telephone's records with country and then cut phone codes from numbers
         */
    }
}
