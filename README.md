Test Tasks
==========

Task 1: PHP/Symfony
-------------------

[Controller Source](src/AppBundle/Controller/TaskController.php)

[Controller Tests](src/AppBundle/Tests/Controller/TaskControllerTest.php)


Task 2: MySQL
-------------

**a) An alphabetically-ordered list of the family names of users with more than one telephone number.**

    SELECT user.family_name
    FROM user WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id) > 1
    ORDER BY user.family_name


**b) A list of the user IDs of users without a telephone number.**

*option 1*
    
    SELECT user.id
    FROM user
    WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id) = 0

*option 2*

    SELECT DISTINCT user.id
    FROM user
        LEFT JOIN telephone ON telephone.user_id = user.id
    WHERE telephone.id IS NULL


**c) A alphabetically reverse-ordered list of the given names of users that have a China telephone number.**

*option 1*

    SELECT user.id
    FROM user
    WHERE (SELECT COUNT(*) FROM telephone WHERE telephone.user_id = user.id AND telephone.telephone LIKE '+86%') > 0
    ORDER BY user.given_name DESC

*option 2*

    SELECT DISTINCT user.id
    FROM user
        INNER JOIN telephone ON telephone.user_id = user.id AND telephone.telephone LIKE '+86%'
    ORDER BY user.given_name DESC


# How can we change the schema to avoid the repetition of country codes in the telephone table?

We can create an additional table `country`
(or maybe `country_phone_code`, but in `country` we can store more country related data)
and add a field named `phone_code`.

Next step to add a field `country_id` to table `telephone`.

After that we can insert countries to country table with their phone codes,
then link telephone's records with country and then cut phone codes from numbers.

Finally, getting telephone numbers with codes could be done:

    SELECT DISTINCT country.phone_code, telephone.telephone
    FROM telephone
        LEFT JOIN country ON country.id = telephone.country_id